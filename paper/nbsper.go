package main

import (
    "regexp"
    "os"
    "io/ioutil"
)

func check(e error) {
    if e != nil {
        panic(e)
    }
}


type re struct {
    regex *regexp.Regexp
    repl string
}

func main () {
    regexes := []re{
        re{regexp.MustCompile(`(\s\w) `), "$1&nbsp;"},
        re{regexp.MustCompile(`(\s+\d+) (\w)`), "$1&nbsp;$2"},
        re{regexp.MustCompile(`(\d) (\d)`), "$1&nbsp;$2"},
    }

    inFileName := os.Args[1]
    outFileName := os.Args[2]

    inFile, err := ioutil.ReadFile(inFileName)
    check(err)
    in := string(inFile)

    for _, regex := range regexes {
        in = regex.regex.ReplaceAllString(in, regex.repl)
    }

    err = ioutil.WriteFile(outFileName, []byte(in), 0644)
    check(err)

}
