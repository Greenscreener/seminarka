window.addEventListener("load", function () {
	// var headings = Array.from(document.all).filter(function (e) {return e.tagName === "H1" || e.tagName === "H2" || e.tagName === "H3"});
	var headings = Array.from(document.getElementsByTagName("*")).filter(function (e) {return e.tagName === "H1" || e.tagName === "H2" || e.tagName === "H3"});
	headings = headings.filter(function (e) {
		return e.getAttribute("class") === null || e.getAttribute("class").indexOf("no-toc") === -1;
	});
	headings.forEach(function (e) {
		var a = document.createElement("a");
		e.setAttribute("id", e.innerHTML.replace(/^\s*/g,"").replace(/\s*$/g,"").replace(/\s/g,"-"));
		a.href = "#" + e.id;
		a.setAttribute("href", "#" + e.id);
		if (e.getAttribute("class") === null || e.getAttribute("class").indexOf("nc") === -1) {
			a.setAttribute("class", "toc-link " + e.tagName);
		} else {
			a.setAttribute("class", "toc-link");
		}
		a.innerHTML = "&nbsp;";
		document.getElementById("toc").appendChild(a);
	});
	
	
	var cites = Array.from(document.getElementsByTagName("cite"));
	cites.sort(function (a, b) {
		var aInnerText = a.innerHTML.replace(/<\/?\w+>/g, "");
		var bInnerText = b.innerHTML.replace(/<\/?\w+>/g, "");
		if (aInnerText < bInnerText) {
			return -1;
		} else if (aInnerText === bInnerText) {
			return 0;
		} else {
			return 1;
		}
	});
	cites.forEach(function (e) {
		if (e.getAttribute("class") === null || e.getAttribute("class").indexOf("rep") === -1) {
			var p = document.createElement("p");
			p.innerHTML = e.innerHTML;
			document.getElementById("literature").appendChild(p);
		}
	});

	document.getElementById("date").innerHTML = /*new Date().getHours() + ":" + new Date().getMinutes() + " " + */new Date().getDate() + ". " + (new Date().getMonth() + 1) + ". " + new Date().getFullYear();

});
