<h1 class="nc">Úvod</h1>

Nedílnou součástí všech múzických umění je technika. Jedná se o světla, zvuk, někdy video a řadu dalších zařízení. Bez této jevištní techniky by byla většina představení téměř neproveditelná. Úkolem technika tedy je, aby při představení ve správnou chvíli svítila ta správná světla, byla otevřená či zavřená opona, hrála ta správná hudba a v neposlední řadě aby také fungovaly ty správné mikrofony. V případě velkých produkcí tuto činnost obhospodařuje často i větší počet profesionálů. 

Mnohdy ale takovou techniku potřebují také menší produkce, pro které není jednoduché si takového profesionála sehnat a tak si musí vystačit sami. Jedním z dobrých příkladů jsou malé divadelní soubory. Potřebují ovládat světla a přehrávat zvuk a tuto funkci většinou obhospodařuje jeden z herců, když zrovna nehraje, podle hektických poznámek zapsaných různými barvami na okraj jednoho výtisku scénáře. 

Proto jsme se v našem týmu, který tvořím já, Vojtěch Káně, Šimon Šustek, Tomáš Kysela a Adam Kadeřávek rozhodli, že se pokusíme této skupině lidí tento problém pomoci vyřešit. Chceme přinést způsob, kterým si mohou tito lidé vytvořit jednoduchý a přehledný scénář, který pochopí kterýkoliv ze členů spolku a zároveň zařízení, které tento scénář interpretuje a s minimálním uživatelským vstupem ho také přehraje. To celé nazýváme Pátek ShowController (PSC). Cílem této práce je představit PSC a popsat jeho vývoj. 

Sami máme z této oblasti zkušenosti - někteří členové týmu ve svém volném čase zvučí a osvětlují menší akce nebo také hrají v divadelním souboru, proto se často s mnohými problémy, které chceme řešit, sami potýkáme. 

Drtivá většina vývoje PSC probíhala během soutěže AT&amp;T Hackaton 2019, kdy jsme měli 24 hodin na vymyšlení a vytvoření nějakého zařízení, o jehož užitečnosti jsme následně museli přesvědčit porotu. Během těchto 24 hodin jsme nestihli vytvořit vše, co jsme si představovali a kromě úspěchů jsme narazili i na mnoho překážek. Ve vývoji hodláme dále pokračovat a dovést k dokonalosti mnoho zatím nedokonalých aspektů našeho zařízení.

# Vize

Rozhodli jsme se vyrobit zařízení, které bude dostupné lidem, kteří nejsou profesionálové, ale stejně potřebují ovládat světla a také přehrávat zvuk. Zařízení by proto mělo být:

1. vše v jednom
2. jednoduché
3. automatické
4. rozšiřitelné

## Vše v jednom

PSC je, na rozdíl od dostupných řešení, jedno zařízení, které zvládne ovládat světla, přehrávat zvuk, promítat video a případně být rozšiřitelné. Čím více techniky, tím je složitější její převoz a více času zabere její příprava. Proto chceme, aby uživatelé mohli připojit všechny kabely pouze do PSC a vše bude fungovat. Jedinou funkci, kterou PSC v současné podobě nezastane, je mix zvuku, jelikož se jedná o problém mimo rozsah našeho projektu a jeho řešení by vyžadovalo významné hardwarové i softwarové rozšíření PSC.

## Jednoduché

Zařízení by mělo mít jednoduché a intuitivní uživatelské rozhraní, které je schopen ovládat i člověk, který se v oblasti jevištní techniky obvykle nepohybuje. Ovládání zařízení je proto rozděleno do dvou fází - fáze Přípravy a fáze Provedení. Přípravu zvládne pokročilejší uživatel, zatímco pro ovládání zařízení při samotném představení není třeba zvláštních dovedností, pouze základní znalosti práce s počítačem. 

Vystoupení mnohdy závisí na jednom člověku, který umí ovládat techniku, a když není k dispozici, může akce pouze obtížně proběhnout.  Chceme, aby v případě potřeby bylo možné nahradit tohoto technika jiným členem souboru, který nikdy předtím takovou funkci nezastával, a tento člen mohl tuto funkci zastat bez stresu a ve stejné kvalitě. 

## Automatické

Jedním z našich cílů je, aby bylo možné vytvořit si scénář, do kterého lze vložit všechny funkce zařízení tak, aby byl operátor schopen za pomoci webového rozhraní spouštět tyto funkce ve stejném pořadí, v jakém byly definovány v tomto scénáři. Forma scénáře byla také vybrána proto, že je již v divadelním prostředí hojně využívaná a kdokoliv, kdo bude ovládat techniku, je s ní obeznámen. 

Když bude pořadí jednotlivých akcí uloženo v zařízení, nebude si je muset operátor pamatovat a tím se mnohonásobně zmenší prostor pro lidskou chybu. I zkušený technik ocení, když nemusí neustále hledat ve scénáři, co přesně má dělat. 

## Rozšiřitelné

Dalším naším cílem je vytvořit zařízení, které bude možné i později rozšířit a přidat do něj nové funkce, které mohou být naimplementovány v novějších verzích. Chceme, aby se zařízení, které je už v rukou uživatelů, nepřestávalo vyvíjet a aby mohli sami uživatelé navrhovat či vytvářet nové funkce.

# Existující jevištní technika

## Ovládání světel a dalších zařízení

Na naprosté většině jevišť je ovládání světel a dalších zařízení jako například mlhovačů, opony, atd. prováděno pomocí protokolu DMX512. Pro komunikaci po tomto protokolu se obvykle používají osvětlovačské pulty. Ovládání probíhá pomocí přiřazení jednotlivých úseků DMX512 (viz následující kapitolu) k posuvníkům (faderům). Jejich pozice určuje hodnotu, která se do přiřazeného úseku zapíše. Složitější pulty také umí vytvářet automatické animace a přiřadit spuštění těchto animací k jiným posuvníkům.

### DMX512

Tento protokol umožňuje vytvořit „řetěz“ zařízení, mezi kterými vede pouze jeden kabel (viz <a href="#DMX-network" class="fig"></a>)

Datová struktura tohoto protokolu je velmi jednoduchá. Každý paket tohoto protokolu obsahuje 512 úseků, každý z těchto úseků obsahuje číslo od 0 do 255.<cite>*ANSI E1.11 – 2008 (R2018): Entertainment Technology—USITT DMX512-A* [online]. 2018-05-31, <span class="pr">, s. 27-29 </span> [cit. 2020-01-10]. Dostupné z: https://tsp.esta.org/tsp/documents/docs/ANSI-ESTA_E1-11_2008R2018.pdf</cite> Tyto pakety jsou vysílány všem zařízením v řádech desítek až stovek opakování za sekundu. 

Díky jednoduchosti protokolu DMX512 jej každé zařízení interpretuje trochu jinak. Zpravidla má na sobě nějaký způsob nastavení své počáteční adresy, tedy pořadí prvního úseku, ze kterého si zařízení bere informace. Pomocí nastavení této adresy je možné rozdělit dostupných 512 úseků mezi dostupná zařízení. Nejjednodušší zařízení, jako například stmívače, které vyžadují pouze jednu hodnotu, obvykle množství vyzářeného světla, využívají pouze jeden úsek, tedy ten, jehož pořadí je definováno nastavenou adresou. Světla, která vyzařují světlo jakékoliv barvy, obvykle využívají tři úseky, a to pro jas červené, zelené a modré barvy, první z nich je znovu definován nastavenou adresou. Existují ale mnohem složitější zařízení, která využívají desítky po sobě jdoucích úseků.  

## Zvuk

Ovládání zvuku se dá rozdělit na dvě základní činnosti. Přehrávání a mix zvuku. 

### Přehrávání zvuku

Mnoho představení má hudební či jiný zvukový podkres, který je přehrávaný elektronicky. Přehrává se obvykle z počítače pomocí specializovaných programů. 

### Mix zvuku

V případě, že se při vystoupení využívá elektronického zesílení zvuku např. pomocí mikrofonů, je třeba všechny tyto zvukové vstupy regulovat a odeslat do správných výstupů pomocí mixážních pultů.

## Promítání

Kromě přehrání zvuku také někdy probíhá během představení promítání obrazu. Toto promítání se také obvykle provádí za pomoci počítače. 

# Uživatelské rozhraní PSC

Jednou z hlavních výhod a také hlavní myšlenkou PSC je jednoduché a intuitivní ovládání. Použití našeho zařízení je rozděleno do dvou fází, Přípravy a Provedení. Fáze Přípravy je složitější a vyžaduje, aby měl uživatel základní znalosti o tom, jak funguje jevištní technika a také o tom, jaké vlastnosti má technika dostupná na místě vystoupení. Fáze Provedení by měla být dostatečně jednoduchá na to, aby byl schopen zařízení během této fáze ovládat i uživatel s pouze základními znalostmi ovládání osobního počítače.

PSC je po fyzické stránce pouze krabička s porty, nemá na sobě žádné ovládací prvky. Uživatelské ovládací rozhraní, které je součástí PSC, je webová aplikace, kterou si po připojení k PSC po síti otevře uživatel na svém notebooku, tabletu nebo mobilu. 

## Fáze Přípravy

Během fáze Přípravy je nutné provést tyto činnosti:

- připravit Profil jeviště
- připravit Scénář

Profil jeviště obsahuje informace o dostupné jevištní technice na daném místě. Scénář obsahuje textový scénář představení a také časovou souslednost všech akcí spouštěných prostřednictvím PSC, které mají během představení proběhnout. 

### Profil jeviště

Každé místo, kde může probíhat vystoupení, je jiné. Rozdíl, který je pro PSC nejdůležitější, je odlišnost světel, která jsou na daném místě využívána. 

Aby bylo možné používat stejný Scénář na více různých místech, je třeba pro každé místo zaznamenat, jaká zařízení jsou dostupná na daném jevišti, jaké mají adresy (viz <a href="#DMX512" class="sec3"></a>), kolik úseků zabírají a jakým způsobem tyto úseky využívají. Také je užitečné zaznamenat skupiny světel, například dle jejich umístění.

Většina na trhu dostupných ovládacích pultů a zařízení pro osvětlování obsahuje vlastní rozsáhlou knihovnu profilů zařízení.<cite>*Avolites Personality Library* [online]. [cit. 2020-01-11]. Dostupné z: https://personalities.avolites.com/</cite><span> </span> Nemáme prostředky na tvorbu vlastní takové knihovny, ale do zařízení plánujeme naimplementovat možnost stáhnout profily z OpenFixtureLibrary<cite>*Open Fixture Library* [online]. [cit. 2020-01-11]. Dostupné z: https://open-fixture-library.org/</cite>, případně importovat z několika dalších podporovaných formátů. 

Díky Profilu jeviště je možné ve Scénáři například uvést, že všechna světla mají být červená, či že má svítit skupina světel, která svítí na jeviště. Informace o tom, jak rozsvítit světla červeně, či která světla jsou v dané skupině, je součástí Profilu jeviště.

Během fáze Přípravy je tedy nutné tento Profil jeviště definovat. 

### Scénář

Během fáze Přípravy je třeba vytvořit Scénář, podle kterého bude představení probíhat. 

Prvním krokem je vložení existujícího textového scénáře do webové aplikace. Webová aplikace podporuje formátování pomocí značkového jazyka Markdown. Do formátu jazyka Markdown je možné soubory jednoduše převést z tradičních formátů jako Microsoft Word, Libreoffice Writer aj. pomocí převodníků, či rozšíření prohlížeče.<cite>*Word to Markdown Converter* [online]. [cit. 2020-01-11]. Dostupné z: https://word2md.com/</cite><cite>*Paste as Markdown* [online]. [cit. 2020-01-11]. Dostupné z: http://markitdown.medusis.com/</cite> V budoucnu by se mohlo také jednat o zabudovanou funkci zařízení. 

Dalším krokem je vložení interaktivních tlačítek do textu. Pomocí nabídky uživatel nastaví parametry operace a tlačítko poté vloží do textu Scénáře. Tlačítka je také možné seskupit, aby bylo možné provést několik akcí ve stejnou chvíli.  

Při vkládání tlačítek do textu také uživatel nahraje všechny podklady pro přehrávání (jako například audio či video soubory) do PSC. Tyto podklady proto nejsou svázány se zařízením uživatele, které je použito k ovládání PSC, nýbrž s PSC samotným.

Viz <a href="#screenshot1" class="fig"></a>, <a href="#screenshot2" class="fig"></a>.

### Markdown

Markdown je značkovací jazyk, který je určený primárně pro tvorbu webového obsahu. Jeho hlavní odlišnost od ostatních značkovacích jazyků je, jak uvádí jeho původní autor John Gruber: <q>Dokument psaný v Markdownu by měl být publikovatelný tak, jak je, jako prostý text, bez toho, aby bylo poznat, že obsahuje značky nebo formátovací instrukce.</q><cite>GRUBER, John. Markdown. *Daring Fireball* [online]. 2004-12-17 [cit. 2019-12-28]. Dostupné z: https://daringfireball.net/projects/markdown/, vlastní překlad</cite> Formátování v Markdownu je velice jednoduché a soubory jsou čitelné i ve formě prostého textu. Například kurzíva se označuje ohraničením textu jednou hvězdičkou na každé straně, tučný text dvěma hvězdičkami, každý řádek bodového seznamu začíná pomlčkou atd. <span> </span><cite>MACFARLANE, John. *CommonMark Spec* [online]. 2019-04-06 [cit. 2020-01-11]. Dostupné z: https://spec.commonmark.org/0.29/</cite>

Pro použití Markdownu jsme se rozhodli proto, že je široce rozšířený (tato seminární práce je psaná v Markdownu), existuje mnoho řešení pro jeho strojové čtení a zobrazení a také je z naší zkušenosti jednoduše pochopitelný pro většinu uživatelů. Také díky tomu nemusíme vytvářet vlastní editor textu s formátováním, ale můžeme využít tento značkovací jazyk.

## Fáze Provedení

Během fáze Provedení prochází uživatel Scénář a podle dění na jevišti kliká na tlačítka s akcemi, které se následně automaticky provádějí. Další možností je ovládání pomocí mezerníku - při každém stisknutí aplikace automaticky spustí následující akci nebo skupinu akcí. 

## Manuální ovládání

Při Přípravě, ale i při Provedení je vhodné, aby měl uživatel k dispozici možnost ovládat techniku přímo, bez přednastavených tlačítek. Při Přípravě se takový režim dá využít pro vytvoření a vyzkoušení efektů a akcí světel. Při Provedení se využije například při nečekaných změnách či poruchách techniky. Uživatelské rozhraní proto obsahuje také sekci pro manuální ovládání, která je přístupná kdykoliv - při Přípravě i při Provedení. Tato sekce obsahuje nástroje například pro přímé ovládání světel, vypnutí zvukových či obrazových výstupů, upravení hlasitosti v případě, že přednastavená hlasitost není adekvátní a podobně. 

# Technické provedení

## Hardware

Hardwarová stránka PSC se skládá z několika částečně nezávislých komponent. Hlavní řídící jednotkou je Raspberry Pi, k němuž lze po USB připojit libovolné zařízení jako například zvukovou kartu nebo další část PSC - Arduino. Arduino přijímá od Raspberry Pi příkazy a pomocí dvou integrovaných obvodů tyto příkazy posílá po protokolu DMX512 světlům a ostatní technice. K Raspberry Pi je také připojený audio a video výstup. Viz <a class="fig" href="#block-diagram"></a>.

### Raspberry Pi

Nejdůležitější částí našeho zařízení je Raspberry Pi. <q>Raspberry Pi je levný počítač velikosti kreditní karty, který se dá zapojit do počítačového monitoru či televize a používá standartní myš a klávesnici. (...) Je schopný všech činností, které očekáváte od stolního počítače, od prohlížení internetu a přehrávání videí ve vysoké kvalitě po tvorbu tabulek, dokumentů a hraní her.</q><cite>What is a Raspberry Pi? *Raspberry Pi* [online]. [cit. 2019-12-10]. Dostupné z: https://www.raspberrypi.org/help/what-%20is-a-raspberry-pi/, vlastní překlad</cite> Díky tomu je možné, abychom vytvořili řídící program v jakémkoliv programovacím jazyce, který je možné spustit na stolním počítači. Zároveň můžeme webovou aplikaci, pomocí které se zařízení ovládá, nechat běžet na Raspberry Pi bez větších obtíží. Co se týče rozšiřitelnosti, je možné připojit jakékoliv zařízení, které je možné připojit pomocí USB portu.

Právě k Raspberry Pi se uživatel připojí po síti, aby otevřel webové uživatelské rozhraní PSC. Celý backend aplikace (viz <a href="#Backend" class="sec2"></a>) běží právě na Raspberry Pi. Raspberry Pi pomocí tohoto backendu přijímá od uživatele příkazy a vykonává je, popř. přeposílá dále např. do Arduina. Video výstup obhospodařuje HDMI zbudované v Raspberry Pi a audio výstup zvukový jack, také zabudovaný v Raspberry Pi. 

Místo Raspberry Pi je možné použít jakýkoliv jiný mikropočítač jako například Orange Pi či Banana Pi. Raspberry Pi jsme vybrali z důvodu jeho rozšířenosti a dostupnosti, zároveň jsme měli Raspberry Pi k dispozici mezi součástkami, když jsme začali PSC vyvíjet. 

Existuje mnoho verzí tohoto mikropočítače, hlavními rozdíly jsou výkon a dostupnost portů. Čím výkonnější verzi bude PSC obsahovat, tím spolehlivěji bude fungovat. Novější verze, které disponují WiFi kartou také umožňují připojení PSC k síti bez nutnosti připojení po kabelu. 

### Arduino

K Raspberry Pi je přes výše zmíněné USB připojena vývojová deska Arduino Nano. <q>Pomocí Arduina můžeme vytvářet interaktivní objekty. Arduino deska získává údaje od různých snímačů a senzorů (například snímač osvětlení, vzdálenosti nebo jen obyčejné tlačítko) a na základě těchto údajů ovládá nějaké výstupy (rozsvítí LED, zapne světlo nebo motor či jiný fyzický výstup).</q><cite>Co je to Arduino? *Arduino.cz* [online]. [cit. 2019-12-10]. Dostupné z: https://arduino.cz/co-je-to-arduino/</cite> Díky dostupným knihovnám můžeme využít Arduino k vysílání DMX512 příkazů, bez nutnosti psát vlastní implementaci tohoto protokolu. 

Přestože má Raspberry Pi vlastní programovatelné výstupy, k ovládání DMX512 jsme je nepoužili. O DMX512 výstup se stará Arduino. Oddělení této části PSC do samostatného zařízení má několik důvodů.

Prvním důvodem je, že jakýkoliv program na Raspberry Pi běží pod operačním systémem, který přiděluje procesorový čas procesům postupně a nikdy se nestane, že by měl program přístup k procesoru neustále. Protokol DMX512 má pevně daná pravidla časových rozmezí mezi částmi odeslaných dat a zároveň je třeba, aby se data vysílala nepřetržitě. Pokud by se operační systém rozhodl pozastavit proces kdykoliv během tohoto odesílání, data by se nenávratně poškodila. Proto je třeba, aby program odesílající data po DMX512 běžel přímo na procesoru, což je u zařízení jako Arduino možné.

Dalším důvodem je, jak už bylo zmíněno výše, že pro Arduino existují knihovny, které umožňují ovládat zařízení po protokolu DMX512, to znamená, že můžeme pro odesílání příkazů použít už existující kód a nemusíme si psát vlastní. 

V neposlední řadě toto oddělení umožňuje ukládat poslední stav příkazů do Arduina. V případě chyby a následného pádu programu běžícího v Raspberry Pi proto zůstanou všechna zařízení ovládaná po DMX512 v posledním stavu, místo toho, aby se vrátila do stavu výchozího. (Například světla, která svítila zůstanou svítit, místo toho, aby zhasla. Do opětovného spuštění programu na Raspberry Pi nebude možné tento stav změnit, ale alespoň nebude na jevišti tma.) 

### MAX485 a další součástky pro DMX512 výstup Arduina

Aby mohlo Arduino komunikovat se zařízeními po DMX512, musí se signál, který z něj vychází, upravit. O tuto úpravu se postará integrovaný obvod MAX485. Zároveň je užitečné, aby byl výstup DMX512 signálu galvanicky odizolován - v případě poruchy ovládaných zařízení mohou tato zařízení vypustit nebezpečné napětí do těchto výstupů. S galvanickou izolací dojde k poškození pouze součástek, které galvanickou izolaci obhospodařují. Bez ní by mohl být nenávratně poškozen celý PSC a další zařízení k němu přípojená. Proto jsem vytvořil vlastní obvod na základě DMX512 Shield on Matthiase Hertela<cite>DMX Shield for Arduino with isolation. *Mathertel.de* [online]. [cit. 2019-12-10]. Dostupné z: https://www.mathertel.de/Arduino/DMXShield.aspx</cite>.  Viz <a class="fig" href="#pcb-schema"></a>, <a class="fig" href="#pcb-3d"></a>, <a class="fig" href="#proto"></a>.

### Zvukový výstup

Pro zvukový výstup může být využit zvukový jack integrovaný v Raspberry Pi, či v případě potřeby vyšší kvality externí zvuková karta. 

Zvukový výstup, který je dostupný na Raspberry Pi, je pouze základní a při použití s větší aparaturou už je občas slyšitelný šum či rušení. Proto jsme i v první verzi PSC, kde jsme zrovna použili Raspberry Pi s velmi nekvalitním výstupem použili jednoduchou a levnou USB zvukovou kartu, a i ta významně zlepšila kvalitu zvuku. 

### Video výstup

Pro video výstup použijeme HDMI zabudované v Raspberry Pi. Toto rozhraní je plně dostačující a v případě potřeby se dá převodníkem připojit i k ostatním známým video vstupům jako VGA, DVI, DisplayPort a dalším. 

### Konektory

PSC obsahuje mnoho různých konektorů pro připojení všech jeho částí a také ostatní techniky:

- několik USB portů umožňuje připojení Arduina, zvukové karty či dalších zařízení k Raspberry Pi
- napájení obhospodařuje napájecí port Raspberry Pi, tedy MicroUSB B nebo USB C, podle modelu
- pro DMX512 je dostupný XLR3 port, který, byť je jeho použití ve specifikaci DMX512 zakázané<cite class="rep">*DMX512-A* <span class="pr">, s. 12 </span></cite> , je mnohonásobně obvyklejší než specifikací doporučený port XLR5.
- HDMI pro video
- 3,5 mm jack pro zvuk
- RJ-45 pro připojení do kabelové sítě

## Frontend

Software PSC se skládá ze dvou hlavních částí, frontendu a backendu. Frontend je část aplikace, která běží na počítači nebo mobilním zařízení uživatele a obsahuje mimo jiné uživatelské rozhraní. 

Frontend PSC je webová aplikace, to znamená, že není nainstalovaná v počítači uživatele, ale otevírá se pomocí webového prohlížeče. Pro tvorbu webových aplikací se používají jazyky HTML, CSS a JavaScript. 

### HTML

HTML je zkratka pro <q>HyperText Markup Language</q>.  Je to <q>základní jazyk World Wide Webu. HTML bylo původně určeno pro sémantický popis vědeckých dokumentů. Jeho obecná konstrukce však během následujících let umožnila jeho adaptaci pro popis mnoho dalších druhů dokumentů a dokonce také aplikací.</q><cite>*HTML Living Standard* [online]. 2020-01-09 [cit. 2020-01-11]. Dostupné z: https://html.spec.whatwg.org/, vlastní překlad</cite> Drtivá většina webových stránek je napsaná v HTML. Aktuálně uznávaná a používaná verze HTML je HTML5, jedná se o živoucí standard, to znamená, že podoba standardu HTML5 se mění tak, jak se mění jeho implementace. 

V HTML je popsaná základní struktura webové aplikace. Zároveň se jedná o „hlavní“ soubor, v HTML se odkazuje na všechny ostatní soubory. 

### CSS

CSS je zkratka pro <q>Cascading Style Sheets</q>, někdy se také používá české <q>kaskádové styly</q>. <q>Jedná se o stylopis používaný k popisu podání dokumentů napsaných v HTML nebo XML (včetně dialektů XML jako SVG, MathML nebo XHTML). CSS popisuje jak mají být jednotlivé prvky vyobrazeny na obrazovce, papíru, v řeči či ostatní médiích.</q><cite>*CSS: Cascading Style Sheets: Mozilla Developer Network* [online]. 2019-11-08 [cit. 2020-01-11]. Dostupné z: https://developer.mozilla.org/en-US/docs/Web/CSS, vlastní překlad </cite> Aktuálně používaná verze CSS je CSS3, která je rovněž živoucí standard. 

V CSS je popsaná konkrétní podoba jednotlivých prvků stránky, jejich barvy, tvary, pozice atd.  

### JavaScript

<q>JavaScript je lehký, interpretovaný, (...) programovací jazyk (...). Přestože je nejznámější jako skriptovací jazyk pro webové stránky, používá se také v mnoha dalších prostředích...</q><cite>*JavaScript: Mozilla Developer Network* [online]. 2019-12-09 [cit. 2020-01-11]. Dostupné z: https://developer.mozilla.org/en-US/docs/Web/javascript, vlastní překlad</cite> 

JavaScript zajišťuje interaktivitu frontendu, jeho komunikaci s backendem a předávání informací zpět uživateli. 

### Vue.js

Pro značné zjednodušení práce používáme Vue.js. Vue.js je JavaScriptový framework, tedy soubor kódu, který je univerzálně použitelný a rozšiřuje funkcionalitu JS. Velmi významně zjednodušuje tvorbu složitějších webových aplikací, ale také jednoduchých prezentací. Základním principem je přímé spojení dat přístupných z JavaScriptu a prvků na stránce a také tvorba komponent, součástí webové stránky, které jsou na sobě nezávislé a dají se použít kdekoliv v rámci stránky. <cite>*Introduction: Vue.js* [online]. 2019-12-09 [cit. 2020-01-11]. Dostupné z: https://vuejs.org/v2/guide/</cite>

Pro použití tohoto frameworku jsme se rozhodli, protože s ním měl předchozí zkušenosti jeden z členů týmu. Je ale třeba podotknout, že, což by byla pravda i u většiny ostatních frameworků, byť hlavně na začátku tento nástroj vývoj velmi podstatně zjednodušil a v konečném důsledku ušetřil mnoho práce, mnoho práce nám také přidělal. Ve chvíli, kdy bylo potřeba vytvořit funkci, kterou framework neuměl, bylo velmi obtížné ho obcházet a i jednoduché rozšíření zabralo mnoho času a práce.  

## Backend

Backend je část aplikace, která běží přímo na PSC, přijímá komunikaci z frontendu, a provádí potřebné úkony. Také ukládá uživatelská data - Scénáře a Profily, nebo také například zvukové soubory pro přehrání. 

### Go API

Tato část aplikace je napsaná v programovacím jazyku Go. Go je kompilovaný, staticky typovaný jazyk, který je od roku 2007 vyvíjen zaměstnanci společnosti Google a díky otevřenému zdrojovému kódu také členy komunity po celém světě. Cílem Go je zproduktivnit a zpříjemnit vývoj softwaru a odstranit každodenní strasti softwarového vývoje jako například: <q>pomalé sestavení, nekontrolované závislosti, používání různých verzí jazyka, špatné porozumění kódu (nečitelnost, nedokumentovanost atd.) (...) </q><cite>PIKE, Rob. *Go at Google: Language Design in the Service of Software Engineering* [online]. [cit. 2020-01-31]. Dostupné z: https://talks.golang.org/2012/splash.article, vlastní překlad</cite> 

Pro Go jsme se rozhodli také díky tomu, že s ním již máme zkušenost. Kompilovanost je užitečná hlavně pro výkon, statické typování a paměťová bezpečnost umožňují zabraňovat mnoha chybám. Programovací jazyk totiž nemusí tolik „odhadovat“, co se programátor snaží napsat, a více informací o programu musí být explicitně definováno. 

Pro komunikaci backendu s frontendem se používá tzv. REST API. API je obecný název pro část aplikace, která umožňuje jiným aplikacím s ní komunikovat a přenášet data. Název API se také používá pro definici a specifikaci této komunikace.<cite>*What is an API?* [online]. [cit. 2020-01-31]. Dostupné z: https://www.redhat.com/en/topics/api/what-are-application-programming-interfaces</cite> REST je styl tvorby API nejčastěji po protokolu HTTP (protokol používaný pro přenos dat na webu). Klíčovým prvkem je rozdělení dat do zdrojů (resources), přiřazení jednotných identifikátorů těmto zdrojům a určení metod (činností, které provádíme při práci se zdroji).<span> </span><cite>*What is REST* [online]. [cit. 2020-01-31]. Dostupné z: https://restfulapi.net/</cite>

REST API je nejpoužívanější forma API, která je zároveň uzpůsobená tak, aby dobře fungovala právě pro webové aplikace. Další výhodou je, že ji většina programátorů webových aplikací zná a je považovaná za standard pro toto odvětví. 

Toto API je tvořené jedním programem napsaným v Go. Obsahuje tzv. router, který příchozí zprávy přijímá a rozděluje dle jejich typu dalším částem tohoto programu. 

### Struktura API

API je hierarchicky rozděleno na jednotlivé části:

- projekty
	- soubory
	- scénář
- profily jeviště
- příkazy
	- zvuk
		- spustit
		- zastavit
		- nastavit hlasitost
	- DMX512
		- nastavit adresu X na hodnotu Y
	- video
		- spustit
		- zastavit

S rozšířením funkčnosti zařízení se rozšíří také struktura tohoto API. 

### Docker

Aby se nám, minimálně ve fázi vývoje, výrazně ulehčila práce, využíváme program, který se jmenuje Docker. Jedná se o program pro správu kontejnerů. <q>Kontejner je standardní jednotka softwaru, která obsahuje kód a všechny jeho závislosti tak, že aplikace běží rychle a spolehlivě v jakémkoliv prostředí.</q><cite>What is a Container? *Docker* [online]. [cit. 2020-02-01]. Dostupné z: https://www.docker.com/resources/what-container</cite> To nám umožňuje vyvíjet backendovou část aplikace na našich počítačích a potom ji jednoduše a přímočaře spustit na Raspberry Pi, bez nutnosti doinstalace závislostí a zjišťování kompatibility knihoven. 

Nutno dodat, že jsme během vývoje narazili na následující problém: Docker nepodporuje nativně sadu instrukcí procesoru, které používají některé starší modely Raspberry Pi. <cite>Raspberry Pi documentation glossary: Raspberry Pi Documentation. *Raspberry Pi* [online]. [cit. 2020-02-09]. Dostupné z: https://www.raspberrypi.org/documentation/glossary/</cite> Proto je třeba v případě využití těchto starších modelů použít upravené kontejnery právě pro tento set instrukcí. 

<h1 class="nc">Závěr</h1>
Zařízení je zatím ve velmi rané fázi vývoje. Drtivá většina kódu, který tvoří tento projekt je ve fázi prototypu a mnoho funkcí chybí úplně. Tvorba, ukládání a přehrání Scénáře je plně funkční, stejně tak ovládání DMX512 a přehrávání hudby. Chybí dokončit však ještě přehrávání videa a sekce profilu jeviště a manuálního ovládání. Zároveň bude třeba velkou část kódu zpřehlednit a vyčistit a provést testování celého zařízení za účelem nalezení chyb a neočekávaného chování. 

Po hardwarové stránce je aktuální stav díky jednoduchosti zařízení o dost blíže finální podobě. Hardware zařízení je již schopen všechny plánované funkce vykonat a funguje spolehlivě, koncový produkt však bude menší a dosavadní dřevěné tělo zařízení bude nahrazeno jiným materiálem. Viz <a href="#assembled" class="fig"></a>.

Až bude zařízení hotové, osloví nejen malé divadelní soubory, ale i profesionály a nadšence v oboru jevištní techniky, kterým také ulehčí a zpříjemní práci. 