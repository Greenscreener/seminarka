#!/bin/bash

repoDir=$(pwd)
tmpDir=$(mktemp -d)

git log --pretty=%h first-buildable-commit~1..master > $tmpDir/buildable-commits
bcFile=$tmpDir/buildable-commits

cd $tmpDir
git clone $repoDir repo
mkdir $tmpDir/pdfs

for i in $(seq 1 $(cat $bcFile | wc -l));
do
    cd $tmpDir/repo
    commit=$(tac $bcFile | head -n $i | tail -n 1)
    git checkout -f $commit
    cd $(find . -name Makefile | sed 's/Makefile//g' | head -n 1)
    make
    cp index.pdf $tmpDir/pdfs/index\ -\ $i\ -\ $commit.pdf
    cd -
done

echo "$tmpDir/pdfs"
