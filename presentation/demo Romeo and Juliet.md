<psc-interval interval="784" id="red-and-blue">

<psc-action module="dmx" command="set">
{
"address": 1,
"value": 255
}
</psc-action>

<psc-timeout timeout="392">
<psc-action module="dmx" command="set">
{
"address": 2,
"value": 255
}
</psc-action>
</psc-timeout>
</psc-interval>



<psc-interval interval="2000" id="fight">

<psc-action module="dmx" command="set">
{
"address": 1,
"value": 255
}
</psc-action>

<psc-timeout timeout="100">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 0
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="243">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 255
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="343">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 0
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="727">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 255
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="827">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 0
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="1740">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 255
}
</psc-action>
</psc-timeout>

<psc-timeout timeout="1900">
<psc-action module="dmx" command="set">
{
"address": 1,
"value": 0
}
</psc-action>
</psc-timeout>

</psc-interval>

# Romeo and Juliet

Written by **Nika Štěpánková**

Translated by **Aleš Launer**

PSC Script by **Jan Černohorský**

---



*(a street carnival in Verona; the masks are running around forming a serpent-shape line; Rosalind’s chase with Romeo, he catches her)*

*(music)*



<psc-block>
<psc-action module="audio" command="play">
{
"displayName": "01 Stopa 1.mp3",
"file": "01.wav"
}
</psc-action>
<psc-call block-id="red-and-blue"></psc-call>
</psc-block>


<psc-block>
<psc-action module="audio" command="stop">
{
}
</psc-action>
<psc-call block-id="red-and-blue"></psc-call>
</psc-block>




**Romeo**: Rosalind, my love, don’t be running away from me.

**Rosalind**: *(stops, she plays with her fan)*

**Romeo**: Don’t pretend not to understand my heart.

**Rosalind**: *(turning her head, shaking her shoulders)*

**Romeo**: Just one kiss.

**Rosalind**: *(the “no” gesture)*

**Romeo**: Just one kiss, my love.

**Rosalind**: *(the “no” gesture again)*

**Romeo**: Don’t make me beg forever.

**Rosalind**: *(kisses him, starts to laugh and runs away)*

**Romeo**: Rosalind, please, listen to me. *(leaves)*

**Mercutio**: Romeo, you fool!

**Romeo**: Mercutio, I’m in a hurry!

**Mercutio**: This must be love, I think.

**Romeo**: *(coming back)* She’s gone.

**Mercutio**: Who?

**Romeo**: Rosalind.

**Mercutio**: Rosalind, your new love story?

**Romeo**: She doesn’t love me.

**Mercutio**: That’s love. Tender and cruel at the same time. This is my advice: Stop thinking about her.

**Romeo**: To stop thinking about her means to stop thinking altogether.

*(music)*

<psc-action module="audio" command="play">
{
"displayName": "01 Stopa 1.mp3",
"file": "01-2.wav"
}
</psc-action>

**Rosalind**: *(appears in the crowd of the masks)*

**Romeo**: It’s Rosalind. Oh my God, stand by me. *(running away)*

**Mercutio**: Poor blind Romeo. Love has taken his senses away. *(leaves)*

*(stop music)*

<psc-action module="audio" command="stop">
{
}
</psc-action>



**Paris**: *(coming, meets Benvolio, biting his thumb at him)*

**Benvolio**: Are you making fun of me? I mean your thumb.

**Paris**: Don’t be funny. I’m just playing with my fingers. You don’t like it, do you?

**Benvolio**: No. Not at all.

**Paris**: *(the same gesture again)* Is it better now?

**Benvolio**: Stop doing that, sir, or...

**Paris**: Or what? You Montague bastard.

**Benvolio**: This is my answer. *(hits him in the face with his glove)* You Capulet bastard. *(pulls out his rapier)*

**Capulet**: Are you insulting the Capulets? Where is my rapier?

**Lady Capulet**: My lord, please, no violence.

**Capulet**: The rapier speaks louder than words. Look. The old Montague is coming and making fun of me.

**Montague**: Capulet, you bastard. *(wants to pull out his rapier)*

**Lady Montague**: No, stop it. Don’t do that.

**Montague**: Leave me alone.

**Lady Montague**: You aren’t going to fight him.





*(the crowd is divided into two parties; a fight)*

*(music)*

<psc-block>
<psc-action module="audio" command="play">
{
"displayName": "02 Stopa 2.mp3",
"file": "02.wav"
}
</psc-action>
<psc-call block-id="fight"></psc-call>
</psc-block>



**Princess**: *(entering)* Stop!



<psc-block>
<psc-action module="audio" command="stop">
{
}
</psc-action>
<psc-call block-id="fight"></psc-call>
</psc-block>
